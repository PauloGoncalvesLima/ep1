#include "pch.hpp"
#include "mapa.hpp"
#include "funcoesextras.hpp"
using namespace std;


Mapa::Mapa(){}

Mapa::~Mapa(){
  free(mapa);
  free(TemBarco);
}

int Mapa::get_coordenadax(){
  return CoordenadaX;
}

void Mapa::set_mapsize(string arquivo){

  set_fMapInfo(arquivo, &CoordenadaX, &CoordenadaY);

  mapa = new char* [CoordenadaY]; //Alocação dinamica dos mapas 
  TemBarco = new bool* [CoordenadaY];

  if(mapa == nullptr || TemBarco == nullptr){
    cout << "ERRO NA ALOCAÇÃO DE MEMORIA" << endl;
    exit(0);
  }

  for(int i = 0; i < CoordenadaY; i++) {

    mapa[i] = new char [CoordenadaX];
    TemBarco[i] = new bool [CoordenadaX];

    if(mapa[i] == nullptr || TemBarco[i] == nullptr){
      cout << "ERRO NA ALOCAÇÃO DE MEMORIA" << endl;
      exit(0);
    }
  }

  for(int i =0; i < CoordenadaY; i++){
    for(int j =0; j < CoordenadaX; j++){
      TemBarco[i][j] = false;
    }
  }
}

void Mapa::set_maps(char c){ //Overloading
  //Setando o mapa todo como algum char no caso sendo como *
  for (int i = 0; i < CoordenadaY; i++) {
    for (int j = 0; j < CoordenadaX; j++) {
      mapa[i][j] = c;
    }
  }
}

void Mapa::set_maps(char c, int AtaqueX, int AtaqueY){ //Overloading
  //Mudando os 2 mapas ao mesmo tempo.
  mapa[AtaqueX][AtaqueY] = c;
  TemBarco[AtaqueX][AtaqueY] = false;
}

void Mapa::set_map(int x, int y, char c){
  mapa[x][y] = c; //Mudando char especifico no mapa
}

//impreção formatada dos 2 mapas
void Mapa::get_mapa(Mapa *map2, string NomeP1, string NomeP2, int VidaP1, int VidaP2){

  cout<<"\t\t\t\t\t\t       NOME: "<< NomeP1;

  for(unsigned int i =0; i<28-NomeP1.size();i++ ) putchar(32);
  setw(2);
  cout << "VIDA: " << VidaP1<< "/30";

  cout << "\t     NOME: "<< NomeP2;
  for(unsigned int i =0; i<28-NomeP2.size();i++ ) putchar(32); 
  setw(2);
   cout << "VIDA: " << VidaP2<< "/30"<<endl;

  //cout << this->CoordenadaY << endl;

  cout << "\t\t\t\t\t\t       ---------------------------------------------";
  cout << "         ---------------------------------------------" << endl;

  cout << "\t\t\t\t\t\t         |";
  for (int i = 0 ; i < this->CoordenadaX ; i++) {
    cout << setw(3);
    cout << i+1;
  }

  cout << "              |";

  for (int i = 0 ; i < map2->CoordenadaX; i++) {
    cout << setw(3);
    cout << i+1;
  }

  cout << "\n\t\t\t\t\t\t       --+------------------------------------------";
  cout << "         --+------------------------------------------\n";

  for (int i = 0 ; i < this->CoordenadaY ; i++) {
    cout << "\t\t\t\t\t\t       "<< setw(2) << i+1 << "|";
    for (int j = 0 ; j < this->CoordenadaX ; j++) {
      cout << "  "<< this->mapa[i][j];
    }

    cout << "         ";
    cout << "   " << setw(2) << i+1 << "|";

    for (int j = 0 ; j < map2->CoordenadaX ; j++) {
      cout << "  " << map2->mapa[i][j];
    }
    cout << endl;
  }

  cout << endl << endl;
}

char Mapa::get_mapa(int x, int y){
  return mapa[x][y];
}

//Impreção do mapa boleano (Não é usado na main so foi usado para saber se tinha ou não barco no mapa)
void Mapa::get_BoolMapa(Mapa *map2){
  //cout << this->CoordenadaY << endl;
  cout << "\t\t\t\t\t\t       ---------------------------------------------";
  cout << "         ---------------------------------------------" << endl;

  cout << "\t\t\t\t\t\t         |";
  for (int i = 0 ; i < this->CoordenadaX ; i++) {
    cout << setw(3);
    cout << i+1;
  }

  cout << "              |";

  for (int i = 0 ; i < map2->CoordenadaX; i++) {
    cout << setw(3);
    cout << i+1;
  }

  cout << "\n\t\t\t\t\t\t       --+------------------------------------------";
  cout << "         --+------------------------------------------\n";

  for (int i = 0 ; i < this->CoordenadaY ; i++) {
    cout << "\t\t\t\t\t\t       "<< setw(2) << i+1 << "|";
    for (int j = 0 ; j < this->CoordenadaX ; j++) {
      cout << "  "<< this->TemBarco[i][j];
    }

    cout << "         ";
    cout << "   " << setw(2) << i+1 << "|";

    for (int j = 0 ; j < map2->CoordenadaX ; j++) {
      cout << "  " << map2->TemBarco[i][j];
    }
    cout << endl;
  }

  cout << endl << endl;
}

//Setando o mapa boleano em uma posição especifica
void Mapa::set_tembarco(int x, int y, bool state){
  TemBarco[x][y] = state;
}

bool Mapa::get_BoolMapa(int x, int y){
  return TemBarco[x][y];
}


int Mapa::checkBarco(int SizeDoBarco, string orientacao, int posX, int posY){//Checando se o mapa boleano tem algum mapa em determinada posição

  if(CompareString(orientacao, "baixo")){
    for(int tamanho = 0; tamanho<SizeDoBarco;tamanho++){
      if((TemBarco[posX+tamanho][posY])){
       return 0;
     }
   }
   return 1;
 }
 else if(CompareString(orientacao, "cima")){
  for(int tamanho = 0; tamanho<SizeDoBarco;tamanho++){
    if((TemBarco[posX-tamanho][posY])){
      return 0;
    }
  }
  return 1;
  }
else if(CompareString(orientacao, "esquerda")){
  for(int tamanho = 0; tamanho<SizeDoBarco;tamanho++){
    if((TemBarco[posX][posY-tamanho])){
     return 0;
   }
 }
 return 1;
  }

else if(CompareString(orientacao, "direita")){
  for(int tamanho = 0; tamanho<SizeDoBarco;tamanho++){
    if((TemBarco[posX][posY+tamanho])){
     return 0;
   }
 }
 return 1;
  }

  else if(CompareString(orientacao, "nada")){
    if((TemBarco[posX][posY])){
     return 0;
   }
 return 1;
  }
return 0;
}


