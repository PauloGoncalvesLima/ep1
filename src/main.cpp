#include "pch.hpp" //Pre Compiled Header


#include "barcos.hpp"
#include "PortaAvioes.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "funcoesextras.hpp"
#include "mapa.hpp"
#include "Player.hpp"



using namespace std;
namespace fs = experimental::filesystem;

template <class T1>
  T1 inputCheck(){
    T1 input;
    cin >> input;
    while(cin.fail()){
      cin.clear();
      cin.ignore(numeric_limits<int>::max(),'\n');
      clear();
      cout << "Erro no input." << endl << "Insira novamente." << endl;
      cin >> input;
    }
    return input;
  }

int main(){

  

  int opcao, P1AtaqueX, P1AtaqueY, P2AtaqueX, P2AtaqueY, Partida = 0;
  string NomeP1, NomeP2;
  clear();
  //Iniciando os mapas
  Mapa * mapa1 = new Mapa();
  Mapa * mapa2 = new Mapa();
  //Iniciando os players 
  Player * P1 = new Player("Mapa 1");
  Player * P2 = new Player("Mapa 2");
  //Criando lista dos barcos
  vector<PortaAvioes *> ListaDePortaAvioes;
  vector<Canoa *> ListaDeCanoas;
  vector<Submarino *> ListaDeSubmarinos;

  MENU://Lugar para o retorno do goto.
  opcao = inicializarMenu();


  switch (opcao) {
    case 1:
    NovoJogo(1, P1, P2, mapa1, mapa2, ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);
    //P2->set_vidaTotal(-30);
    //P1->set_Pontos(10);
    //P2->set_Pontos(10);
    while(P1->get_vidaTotal() > 0 && P2->get_vidaTotal() > 0){
      clear();
      //imprimindo o mapa
      mapa1->get_mapa(mapa2, P1->get_NomeDoJogador(), P2->get_NomeDoJogador(), P1->get_vidaTotal(), P2->get_vidaTotal());

      //mapa1->get_BoolMapa(mapa2);
      if(Partida > 0) {
        P1->Ataque(P1AtaqueX, P1AtaqueY, mapa2, P2, "Mapa 2", ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);//Ataque dos players 
        P2->Ataque(P2AtaqueX, P2AtaqueY, mapa1, P1, "Mapa 1", ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);//Ataque dos players 
        cout << "Pontos P1: " << P1->get_Pontos() << endl;
        cout << "Pontos P2: " << P2->get_Pontos() << endl;
      }

      cout << P1->get_NomeDoJogador() << " sua vez de atacar: X: ";
      P1AtaqueX = inputCheck<int>();
      while(P1AtaqueX < 1 || P1AtaqueX > mapa1->get_coordenadax()){//Limintando para o tamanho do mapa
        cout<<"O mapa não tem esse tamanho" << endl;
          cout << P1->get_NomeDoJogador() << " sua vez de atacar: X: ";
          P1AtaqueX = inputCheck<int>();
      }

      cout << P1->get_NomeDoJogador() << " sua vez de atacar: Y: ";
      P1AtaqueY = inputCheck<int>();
      while(P1AtaqueY < 1 || P1AtaqueY > mapa1->get_coordenadax()){
        cout<<"O mapa não tem esse tamanho" << endl;
          cout << P1->get_NomeDoJogador() << " sua vez de atacar: Y: ";
          P1AtaqueY = inputCheck<int>();
      }
      
      //repetindo o processo para o player2
      cout << P2->get_NomeDoJogador() << " sua vez de atacar: X: ";
      P2AtaqueX = inputCheck<int>();
      while(P2AtaqueX < 1 || P2AtaqueX > mapa2->get_coordenadax()){
        cout<<"O mapa não tem esse tamanho" << endl;
          cout << P2->get_NomeDoJogador() << " sua vez de atacar: X: ";
          P2AtaqueX = inputCheck<int>();
      }

      cout << P2->get_NomeDoJogador() << " sua vez de atacar: Y: ";
      P2AtaqueY = inputCheck<int>();
      while(P2AtaqueY < 1 || P2AtaqueY > mapa2->get_coordenadax()){
        cout<<"O mapa não tem esse tamanho" << endl;
          cout << P2->get_NomeDoJogador() << " sua vez de atacar: Y: ";
          P2AtaqueY = inputCheck<int>();
      }

      if( Partida == 0){
        P1->Ataque(P1AtaqueX, P1AtaqueY, mapa2, P2, "Mapa 2", ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);
        P2->Ataque(P2AtaqueX, P2AtaqueY, mapa1, P1, "Mapa 1", ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);
        cout << "Pontos P1: " << P1->get_Pontos() << endl;
        cout << "Pontos P2: " << P2->get_Pontos() << endl;
      }

      Partida++;
    }
    
    NomeP1 = P1->get_NomeDoJogador();
    NomeP2 = P2->get_NomeDoJogador();

    transform(NomeP1.begin(), NomeP1.end(), NomeP1.begin(), ::toupper);//Colocando em caixa alta os nomes para imprimir na vitoria
    transform(NomeP2.begin(), NomeP2.end(), NomeP2.begin(), ::toupper);//Colocando em caixa alta os nomes para imprimir na vitoria

    if(P1->get_Pontos() > P2->get_Pontos()){

      for(int i =0; i < 63; i++) putchar(32);
      cout << "PARABENS " << NomeP1 << " VOCE GANHOU COM UM TOTAL DE " << NomeP1 << " PONTOS." << endl;
    }

    else if(P1->get_Pontos() < P2->get_Pontos()){
      for(int i =0; i < 63; i++) putchar(32);
      cout << "PARABENS " << NomeP2 << " VOCE GANHOU COM UM TOTAL DE " << NomeP2 << " PONTOS." << endl;
    }
    else{
      cout << "FICOU EMPATADO " << NomeP1 << " E " <<  NomeP2 << " VOCES FIZERAM UM TOTAL DE " << P1->get_Pontos() << " PONTOS." << endl;
    }
    break;

    case 2:
      cin.clear();
      cin.ignore(numeric_limits<int>::max(),'\n');//Ignorando inputs ateriores
      clear();
      Imprimirtexto("./doc/GameText/regras.txt");//Imprindo as regras
      cout << '\n';
      for(int i =0; i < 63; i++) putchar(32);
      cout << "APERTE QUALQUER BOTÃO PARA VOLTAR." << endl;
      cin.clear();
      cin.ignore(numeric_limits<int>::max(),'\n');
      clear();
      goto MENU;//voltando para o começo do menu e reiniciando o menu.
    break;
    case 3:
      cout <<  "SAIR" << endl;
    break;
    default:
    while ((opcao>4) || (opcao<1)) {
      opcao = inicializarMenu();
    }

  }
  return 0;
}
