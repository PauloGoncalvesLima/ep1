#include <chrono>
#include <random>

#include "pch.hpp"
#include "Player.hpp"
#include "funcoesextras.hpp"


class barcos;

using namespace std;

Player::Player(string MapaDoPlayer){
	set_MapaDoPlayer(MapaDoPlayer);
	VidaTotal = 0;
	QuantidadeDeBarcos = 0;
	Pontos = 0;
	NomeDoJogador = "";
}

Player::~Player(){

}


void Player::set_vidaTotal(int VidaTotal){
	this->VidaTotal += VidaTotal;
}
int Player::get_vidaTotal(){
	return VidaTotal;
}

void Player::set_QuantidaDeDeBarcos(int QuantidadeDeBarcos){
	this->QuantidadeDeBarcos += QuantidadeDeBarcos;
}
int Player::get_QuantidadeDeBarcos(){
	return QuantidadeDeBarcos;
}


void Player::set_Pontos(int Pontos){
	this->Pontos += Pontos;
}
int Player::get_Pontos(){
	return Pontos;
}


void Player::set_NomeDoJogador(string NomeDoJogador){
	this->NomeDoJogador = NomeDoJogador;
}
string Player::get_NomeDoJogador(){
	return NomeDoJogador;
}

void Player::set_MapaDoPlayer(string MapaDoPlayer){
	this->MapaDoPlayer = MapaDoPlayer;
}
string Player::get_MapaDoPlayer(){
	return MapaDoPlayer;
}


void Player::set_player(string QualPlayer, string arquivo, Mapa *map,vector<PortaAvioes *>& ListaDePortaAvioes, vector<Canoa *>& ListaDeCanoas, vector<Submarino *>& ListaDeSubmarinos){

	ifstream file(arquivo);
	vector<string> InformacoesBarco;
	string Linha;
	bool InfoFound = false;

	//Lendo informaçãoes do playerX em determinado arquivo.
	if(file.is_open()){

		while(getline(file, Linha)){

			if(CompareString(Linha, QualPlayer)){

				InfoFound = true;

				while (getline(file, Linha)) {

					if(Linha.empty()) {
						while (Linha.empty()) {
							getline(file, Linha);
						}
					}

					if(CompareString(Linha, "#")){
						Linha.clear();
						break;
					}

					boost::split(InformacoesBarco, Linha, [](char c){return c == ' ';});//Fazendo a mesam separação no mapa
					int x, y;
					/*cout << "X: "<< InformacoesBarco[0] << "  Y: "<< InformacoesBarco[1] << endl;
					cout << "Barco: "<< InformacoesBarco[2] << "\nOrientação: "<< InformacoesBarco[3] << endl;*/
					x = stoi(InformacoesBarco[0]);//passando as informaçãoes de string para int
					y = stoi(InformacoesBarco[1]);//passando as informaçãoes de string para int

					if(CompareString(InformacoesBarco[2], "porta-avioes")){
						PortaAvioes * PA = new PortaAvioes("porta-avioes", InformacoesBarco[3], get_MapaDoPlayer()); //Criando um novo barco dependendo do nome dele no arquivo
						ListaDePortaAvioes.push_back(PA);
						set_vidaTotal(4);//Setando a vida total do player
						//cout << "PA push" << endl;
						if(map->checkBarco(ListaDePortaAvioes.back()->get_SizeBarco(), ListaDePortaAvioes.back()->get_OrientacaoBarco(),x, y)){//Checando para ver se tem algum barco ja existente no mapa boleano
							ListaDePortaAvioes.back()->barcos::set_BarcosNoMapa(x, y, map);
						}
					}

					//repetição do processo anterior para o submarino e canoas.
					else if(CompareString(InformacoesBarco[2], "submarino")){
						Submarino * SUB = new Submarino("submarino", InformacoesBarco[3], get_MapaDoPlayer());
						ListaDeSubmarinos.push_back(SUB);
						set_vidaTotal(4);
						//cout << "Sub push" << endl;
						if(map->checkBarco(ListaDeSubmarinos.back()->get_SizeBarco(), ListaDeSubmarinos.back()->get_OrientacaoBarco(), x, y)){
							ListaDeSubmarinos.back()->set_BarcosNoMapa(x, y, map);
						}
					}

					else if(CompareString(InformacoesBarco[2], "canoa")){
						Canoa * CAN = new Canoa("canoa", InformacoesBarco[3], get_MapaDoPlayer());
						ListaDeCanoas.push_back(CAN);
						set_vidaTotal(1);
						//cout << "Canoa push" << endl;
						if(map->checkBarco(ListaDeCanoas.back()->get_SizeBarco(), ListaDeCanoas.back()->get_OrientacaoBarco(),x, y)){
							ListaDeCanoas.back()->set_BarcosNoMapa(x, y, map);
						}
					}
					
				}
				break;
			}
		}

		file.close();
		if(!InfoFound) {
			cout << "Não foi achada nenhuma informação do "<< QualPlayer << " no " << arquivo << "." << endl;
		}
	}
	else cout<< "Não foi possivel abrir o arquivo." << endl;
}


int Player::Ataque(int y, int x, Mapa *map, Player *PlayerAtacado, string MapaAtacado,vector<PortaAvioes *>& ListaDePortaAvioes, vector<Canoa *>& ListaDeCanoas, vector<Submarino *>& ListaDeSubmarinos){

	bool PosFound = false;
	vector<int> Posicoes;
	string NomeDoBarco;
	x -=1;
	y -= 1;

	if(!PosFound){
		//Procurando primeiro em canoas pois tem uma maior chace de achar primeiro.
		//Procurando no vetor de posições de cada canoa no mapa para ver se tem o input de ataque do jogador.
		for(unsigned int i = 0; i < ListaDeCanoas.size(); i++){

			Posicoes = ListaDeCanoas[i]->get_posicao();

			for(unsigned int j = 0; j < Posicoes.size(); j+=2){

				if(x == Posicoes[j] && y == Posicoes[j+1] && (CompareString(ListaDeCanoas[i]->get_Mapa() ,MapaAtacado))){//Checando se a posição existe no mapa que esta sendo atacado
																														 //pois todos os barcos são salvos na mesma lista

					int Vida;
					Vida = ListaDeCanoas[i]->get_vida(j);
					PosFound = true;
					NomeDoBarco = ListaDeCanoas[i]->get_NomeBarco();
					ListaDeCanoas[i]->set_vida(j, Vida-1);//tirando a vida do barco na posição

					if(map->get_BoolMapa( Posicoes[j],  Posicoes[j+1])){//Checando se tem algum barco naquela posição ou se ja foi destruido.

						PlayerAtacado->set_vidaTotal(-1);//tirando a vida do player
						map->set_maps('@',Posicoes[j],Posicoes[j+1]);//Mudando o char no mapa
						set_Pontos(5);//Amentando os pontos
						cout << get_NomeDoJogador() <<" voce detruiu uma Canoa na posição: X: " << Posicoes[j]+1 << " e Y: " << Posicoes[j+1]+1 << "." << endl;
						
					}
					return 1;
				}
			}
		}
	}

	//repetição do processo anterior mas para poder trocar a o char no mapa se deve destruir 2 vezes o mesmo lugar.
	if(!PosFound){
		for(unsigned int i = 0; i < ListaDeSubmarinos.size(); i++){
			Posicoes = ListaDeSubmarinos[i]->get_posicao();
			for(unsigned int j = 0; j < Posicoes.size(); j+=2){
				if(x == Posicoes[j] && y == Posicoes[j+1] && (CompareString(ListaDeSubmarinos[i]->get_Mapa() ,MapaAtacado))){
					PosFound = true;
					int Vida;
					Vida = ListaDeSubmarinos[i]->get_vida(j);
					NomeDoBarco = ListaDeSubmarinos[i]->get_NomeBarco();
					ListaDeSubmarinos[i]->set_vida(j, Vida-1);

					//cout <<"Y: "<< Posicoes[j] << " X: "<< Posicoes[j+1] <<" Vida: " <<  ListaDeSubmarinos[i]->get_vida(j) << endl;

					if(map->get_BoolMapa( Posicoes[j],  Posicoes[j+1]) && (map->get_mapa(Posicoes[j],Posicoes[j+1]) == '*')){
						PlayerAtacado->set_vidaTotal(-1);
						map->set_map(Posicoes[j], Posicoes[j+1],'S');
						cout << get_NomeDoJogador() <<" voce acertou um " << NomeDoBarco << " na posição: X: " << Posicoes[j]+1 << " e Y: " << Posicoes[j+1]+1 << "." << endl;
					}
					else if(map->get_BoolMapa( Posicoes[j],  Posicoes[j+1])){
						PlayerAtacado->set_vidaTotal(-1);
						map->set_maps('@',Posicoes[j],Posicoes[j+1]);
						set_Pontos(5);
						cout << get_NomeDoJogador() <<" voce detruiu um " << NomeDoBarco << " na posição: X: " << Posicoes[j]+1 << " e Y: " << Posicoes[j+1]+1 << "." << endl;
					}
					return 1;
				}
			}
		}
	}

	//Repetindo o processo novamente para os porta avioes
	if(!PosFound){
		
		for(unsigned int i = 0; i < ListaDePortaAvioes.size(); i++){
			Posicoes = ListaDePortaAvioes[i]->get_posicao();
			for(unsigned int j = 0; j < Posicoes.size(); j+=2){
				if(x == Posicoes[j] && y == Posicoes[j+1] && (CompareString(ListaDePortaAvioes[i]->get_Mapa() ,MapaAtacado))){
					int Vida;
					unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
					minstd_rand0 NumRand (seed);
					NomeDoBarco = ListaDePortaAvioes[i]->get_NomeBarco();
					Vida = ListaDePortaAvioes[i]->get_vida(j);

					if(NumRand() > NumRand.max()*(0.30) && (map->get_BoolMapa( Posicoes[j],  Posicoes[j+1]))){//30% de chance de não ser destruido.

						ListaDePortaAvioes[i]->set_vida(j, Vida-1);
						map->set_maps('@',Posicoes[j], Posicoes[j+1]);

						cout << get_NomeDoJogador() <<" voce detruiu um " << NomeDoBarco << " na posição: X: " << Posicoes[j] +1<< " e Y: " << Posicoes[j+1]+1 << "." << endl;
						set_Pontos(10);
						PlayerAtacado->set_vidaTotal(-1);
						return 1;

					}
					else if((map->get_BoolMapa( Posicoes[j],  Posicoes[j+1]))){

						map->set_map(Posicoes[j], Posicoes[j+1], 'P');
						cout << get_NomeDoJogador() <<" voce acertou um " << NomeDoBarco << " na posição: X: " << Posicoes[j]+1 << " e Y: " << Posicoes[j+1]+1 << "." << endl;
						cout << "Mas ele desviou o seu tiro e não foi destruido." << endl;
						return 0;

					}
					else{

						cout << "O "<< NomeDoBarco <<" que tinha na posição: X: " << Posicoes[j]+1  << " e Y: " << Posicoes[j+1]+1 << " já foi destruido "<< get_NomeDoJogador() <<"." << endl;
						return 0;

					}
				}
			}
		}
	}
	//caso não tenha nada mudar para outro char
	if((map->get_mapa( x,  y) == '*')) {
		map->set_map(x, y,'~');
		cout << get_NomeDoJogador() <<" voce não acertou nenhum barco na posição: X: " << x+1 << " e Y: " << y+1 << "." << endl;
		return 0;
	}
	else if(map->get_mapa( x,  y) == '@'){
		cout << "O barco que tinha nessa posição: X: " << x+1 << " e Y: " << y+1 << " já foi destruido "<< get_NomeDoJogador() <<"." << endl;
		return 0;
	}
	return 0;
}