#include "pch.hpp"
#include "funcoesextras.hpp"
using namespace std;
namespace fs = experimental::filesystem;


void clear(){

  system("clear||cls");
}

int CompareString(string StrToComp, string StrComp){
  if(StrToComp.compare(0,StrComp.length(),StrComp) == 0 || StrToComp.compare(StrComp) == 0 || StrToComp == StrComp){
    return 1;
  }
  else{
    return 0;
  }
}

template <class T1>//template para erro no input
T1 inputCheck(){
  T1 input;
  cin >> input;
  while(cin.fail()){
    cin.clear();
    cin.ignore(numeric_limits<int>::max(),'\n');
    clear();
      for(int i =0; i < 63; i++) putchar(32);
    cout << "+ ------------------------------------ +"<< endl;
  for(int i =0; i < 63; i++) putchar(32);
    cout << "|        Insira uma opção valida.      |"<< endl;
  for(int i =0; i < 63; i++) putchar(32);
    cout << "+ ------------------------------------ +"<< endl;
  for(int i =0; i < 63; i++) putchar(32);
    cout << "~ ";
    cin >> input;
  }
  return input;
}

void Imprimirtexto(string arquivo){

  ifstream file(arquivo); //Abrindo o arquivo
  string Linha;

  if(file.is_open()){

    while ( getline(file, Linha) )
    {
      cout << Linha << endl; //Imprimindo toda as linhas do arquivo
    }

    file.close();
  }

  else cout<< "Não foi possivel abrir o arquivo." << endl;

}

void set_fMapInfo(string arquivo, int *MapSizeX, int *MapSizeY){

  ifstream file(arquivo);
  vector<string> StringSeparada;
  string Linha;
  bool InfoFound = false;

  if(file.is_open()){
    while(getline(file, Linha)){

      if(CompareString(Linha, "# Map_Config")){
        InfoFound = true;
        getline(file, Linha);

        if(Linha.empty()) Linha.clear(); //Limpando linhas em branco para não dar erro

        boost::split(StringSeparada, Linha, [](char c){return c == ' ';}); //Separando a string lida por espaços ex: 
                                                                           //1 0 Canoa cima <- essa string
                                                                           //fica assim:
                                                                             //1
                                                                             //0
                                                                             //Canoa
                                                                             //cima

        *MapSizeX = stoi(StringSeparada[0]); //Inserindo o tamanho do mapa nas coordenadasX do mapa por referencia
        *MapSizeY = stoi(StringSeparada[1]); //Inserindo o tamanho do mapa nas coordenadasY do mapa por referencia

        break;
      }

    }

    if(!InfoFound) {
      std::cout << "Não foi possivel achar alguma informação sobre o mapa neste arquivo." << '\n';
      std::cout << "Setado o tamanho do mapa como 13." << '\n';
      *MapSizeX = 13;
      *MapSizeY = 13;
    }
    file.close();

  }
  else cout<< "Não foi possivel abrir o arquivo." << endl;
}

void imprimir_arquivos(string path, string delimitador, vector<string> *ComparadorDeOp){
  vector<string> caminho;

  for (const auto & entry : fs::directory_iterator(path)){
    caminho.push_back(entry.path());//Pegando o nome dos arquibos em determinado caminho
  }

  //Imprimindo de formalizada os mapas
  for(int i =0; i < 63; i++) putchar(32);
  cout << "+------------------------------+" << endl;
  for(int i =0; i < 63; i++) putchar(32);
  cout << "|             MAPAS            |" << endl;
  for(int i =0; i < 63; i++) putchar(32);
  cout << "+------------------------------+" << endl;

  for(unsigned int i = 0; i < caminho.size() ; i++) {
    ComparadorDeOp->push_back(caminho[i].erase(0, delimitador.length()));
    for(int i =0; i < 63; i++) putchar(32);
    cout << "|          • ";
    cout << caminho[i];
    cout << "          |" << endl;
  }
  for(int i =0; i < 63; i++) putchar(32);
  cout << "+------------------------------+" << endl;
}


string SelecionarOMapa(){

  vector<string> ChecarOp;
  string mapa;
  string caminho;
  bool saoIguais;

  saoIguais = false;

  imprimir_arquivos("./doc/Mapas","./doc/Mapas/", &ChecarOp);//Imprime os mapas

  for(int i =0; i < 63; i++) putchar(32);
  cout << "~ Escolha um mapa: ";
  mapa = inputCheck<string>(); //Escolhendo o mapa

  for(unsigned int i = 0; i < ChecarOp.size();i++){//Check para ver se tem algum mapa igual ao escolhido
    if(ChecarOp[i].compare(mapa) == 0){
      saoIguais = true;
      caminho = "./doc/Mapas/" + mapa;
    }
  }

  while(!saoIguais){

    clear();

    cout << "Não tem nenhum mapa com esse nome: " << mapa << "."<< endl;

    for(unsigned int i =0; i < ChecarOp.size();i++){
      cout << ChecarOp[i] << endl;
    }

    cout << "Insira um novo mapa: ";
    mapa = inputCheck<string>();

    for(unsigned int i = 0; i < ChecarOp.size();i++){
      if(ChecarOp[i].compare(mapa) == 0){
        saoIguais = true;
        caminho = "./doc/Mapas/" + mapa;
        break;
      }
    }
  }
  //cout << caminho << endl;
  return caminho;//returnando o caminho completo
}

int inicializarMenu(){

  int OpcaoInicial;
  Imprimirtexto("./doc/GameText/inicio.txt");
  cout << "\t\t\t\t\t\t\t       ~ Digite sua opção: ";

  OpcaoInicial = inputCheck<int>();

  while ((OpcaoInicial > 3 || OpcaoInicial < 1)) {

    clear();
    Imprimirtexto("./doc/GameText/inicio.txt");
    cout << "\t\t\t\t\t\t\t       ~ Digite sua opção: ";
    OpcaoInicial = inputCheck<int>();

  }
  return OpcaoInicial;
}

void NovoJogo(int OpcaoDeNovoJogo, Player *P1,Player *P2,Mapa *mapa1, Mapa *mapa2, vector<PortaAvioes *>& ListaDePortaAvioes, vector<Canoa *>& ListaDeCanoas, vector<Submarino *>& ListaDeSubmarinos){


  string NomeTemp;

  string MapaEscolhido;
  switch (OpcaoDeNovoJogo) {
    case 1:
    MapaEscolhido = SelecionarOMapa();//Escolhendo o mapa para ser aberto
    mapa1->set_mapsize(MapaEscolhido);//Abrindo o mapa e setando o tamanho dele
    mapa2->set_mapsize(MapaEscolhido);//Abrindo o mapa e setando o tamanho dele
    //cout << mapa2->get_coordenadax();
    mapa1->set_maps('*');//inicializando o mapa com * nele todo.
    mapa2->set_maps('*');//inicializando o mapa com * nele todo.
    P1->set_player("# player_1",MapaEscolhido, mapa1, ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);//Setando o player.
    P2->set_player("# player_2",MapaEscolhido, mapa2, ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);//Setando o player.
    //Ignorando qualquer input anterior para poder ler o nome do player.
    cin.clear();
    cin.ignore(10,'\n');

    cout << "Insira o nome do Primeiro jogador: ";  
    getline(cin, NomeTemp);//Pegando toda a linha e salvando no nome temporario para poder checar se o nome não tem mais q 25 caracteres

    while(NomeTemp.size()>25){
      clear();
      cout << "Insira um nome de ate 25 caracteres." << endl;
      cout << "Insira o nome do Primeiro jogador: ";
      getline(cin, NomeTemp);
    }
    P1->set_NomeDoJogador(NomeTemp);//Setando o nome do Player 1


    //Repetindo o processo para o player 2.
    cout << "Insira o nome do Segundo jogador: ";
    while(NomeTemp.size()>25){
      clear();
      cout << "Insira um nome de ate 25 caracteres." << endl;
      cout << "Insira o nome do Segundo jogador: ";
      getline(cin, NomeTemp);
    }
    getline(cin, NomeTemp);
    P2->set_NomeDoJogador(NomeTemp);
    break;
    case 2:
      //Casos não feitos
    break;
    case 3:
      //Casos não feitos
    break;
    default:
      cout <<"ERRO"<<endl;
      NovoJogo(OpcaoDeNovoJogo, P1, P2, mapa1, mapa2, ListaDePortaAvioes, ListaDeCanoas, ListaDeSubmarinos);
  }
}
