#include "pch.hpp"
#include "barcos.hpp"

using namespace std;

barcos::barcos(){ 
	NomeBarco = "";
	OrientacaoBarco = "";
	SizeBarco = 0;
	Map = "";
}

barcos::~barcos(){}



void barcos::set_vida(int vida){
	for(int i = 0; i < get_SizeBarco(); i++){
		this->vida.push_back(vida);
	}
}

void barcos::set_vida(int VecPos, int vida){
	this->vida[VecPos] = vida;
}

int barcos::get_vida(int VecPos){
	return vida[VecPos];
}

unsigned int barcos::get_SizeVida(){
	return vida.size();
}



void barcos::set_SizeBarco(int SizeBarco){
	this->SizeBarco = SizeBarco;
}
int barcos::get_SizeBarco(){
	return SizeBarco;
}

void barcos::set_posicao(int x, int y){
	posicao.push_back(x);
	posicao.push_back(y);
}

void barcos::set_posicao(int VecPos, unsigned int value){
	posicao[VecPos] = value;
}

vector<int> barcos::get_posicao(){
	return posicao;
}

unsigned int barcos::get_SizePosicao(){
	return posicao.size();
}


void barcos::set_NomeBarco(string NomeBarco){
	this->NomeBarco = NomeBarco;
}
string barcos::get_NomeBarco(){
	return NomeBarco;
}



void barcos::set_OrientacaoBarco(string OrientacaoBarco){
	this->OrientacaoBarco = OrientacaoBarco;
}
string barcos::get_OrientacaoBarco(){
	return OrientacaoBarco;
}


void barcos::set_Mapa(string Map){
	this->Map = Map;
}
string barcos::get_Mapa(){
	return Map;
}


void barcos::set_BarcosNoMapa(int posX, int posY, Mapa *Map){

	if(OrientacaoBarco.compare(0,OrientacaoBarco.length(),"cima") == 0
		|| OrientacaoBarco.compare("cima") == 0
		|| OrientacaoBarco == "cima"){
		for(int tamanho = 0; tamanho < get_SizeBarco();tamanho++){
			set_posicao(posX-tamanho, posY); //Salvando as posições do barco em um vector
			Map->set_tembarco(posX-tamanho, posY, true); //Setando no mapa booleano
			Map->set_map(posX-tamanho, posY, '*');
		}
	}

	//Repetição do de cima para as outras orientações.
	else if(OrientacaoBarco.compare(0,OrientacaoBarco.length(),"baixo") == 0
		|| OrientacaoBarco.compare("baixo") == 0
		|| OrientacaoBarco == "baixo"){
		for(int tamanho = 0; tamanho< get_SizeBarco();tamanho++){
			set_posicao(posX+tamanho, posY);
			Map->set_tembarco(posX+tamanho, posY, true);
			Map->set_map(posX+tamanho, posY, '*');
		}
	}
	else if(OrientacaoBarco.compare(0,OrientacaoBarco.length(),"direita") == 0
		|| OrientacaoBarco.compare("direita") == 0
		|| OrientacaoBarco == "direita"){
		for(int tamanho = 0; tamanho<get_SizeBarco();tamanho++){
			set_posicao(posX, posY+tamanho);
			Map->set_tembarco(posX, posY+tamanho, true);
			Map->set_map(posX, posY+tamanho, '*');
		}
	}
	else if(OrientacaoBarco.compare(0,OrientacaoBarco.length(),"esquerda") == 0
		|| OrientacaoBarco.compare("esquerda") == 0
		|| OrientacaoBarco == "esquerda"){
		for(int tamanho = 0; tamanho<get_SizeBarco();tamanho++){
			set_posicao(posX, posY-tamanho);
			Map->set_tembarco(posX, posY-tamanho, true);
			Map->set_map(posX, posY-tamanho, '*');
		}
	}

	else if(OrientacaoBarco.compare(0,OrientacaoBarco.length(),"nada") == 0
		|| OrientacaoBarco.compare("nada") == 0
		|| OrientacaoBarco == "nada"){
			set_posicao(posX, posY);
			Map->set_tembarco(posX, posY, true);
			Map->set_map(posX, posY, '*');
	}
}