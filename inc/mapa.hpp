#ifndef MAPA_HPP
#define MAPA_HPP

#include "pch.hpp"

using namespace std;

class Mapa{

  private:
    //Mapas
    char **mapa;
    bool **TemBarco;

    int CoordenadaX, CoordenadaY;
    int Energia, BarcosRestantes;

  public:
    //Metodos construtora e destrutoras
    Mapa();

    ~Mapa();

    //Metodos Auxiliares
    int get_coordenadax();

    void set_mapsize(string arquivo);
    void set_maps(char c);
    void set_maps(char c, int AtaqueX, int AtaqueY);

    void set_map(int x, int y, char c);

    void get_mapa(Mapa *map2, string NomeP1, string NomeP2, int VidaP1, int VidaP2);
    char get_mapa(int x, int y);
    void get_BoolMapa(Mapa *map2);

    bool get_BoolMapa(int x, int y);
    
    void set_tembarco(int x, int y, bool state);
    
    //Outros Metodos
    int checkBarco(int SizeDoBarco, string orientacao, int posX, int posY);
};

#endif
