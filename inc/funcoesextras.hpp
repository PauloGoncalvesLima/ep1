#ifndef FUNCOESEXTRAS_HPP
#define FUNCOESEXTRAS_HPP

#include "pch.hpp"

#include "PortaAvioes.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"


#include "mapa.hpp"

#include "Player.hpp"

using namespace std;

//Biblioteca para algumas funções extras na main ou em outras partes do codico.

void clear();
int CompareString(string StrToComp, string StrComp); //Função para comparar strings.
void Imprimirtexto(string arquivo);//Imprimir algum arquivo de texto.


void set_fMapInfo(string arquivo, int *MapSizeX, int *MapSizeY); //Pegar informações sobre o mapa nos arquivos.
void imprimir_arquivos(string path, string delimitador, vector<string>* ComparadorDeOp);//Imprimir os mapas que tem na pasta de mapas.


string SelecionarOMapa();//Função para abrir o mapa e ler esse mapa.


int inicializarMenu();//Função para começar o menu.

//Inicinado o jogo.
void NovoJogo(int OpcaoDeNovoJogo, Player *P1,Player *P2,Mapa *mapa1, Mapa *mapa2, vector<PortaAvioes *>& ListaDePortaAvioes, vector<Canoa *>& ListaDeCanoas, vector<Submarino *>& ListaDeSubmarinos);

#endif
