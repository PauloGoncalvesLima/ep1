#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "pch.hpp"
#include "barcos.hpp"

using namespace std;
//Classe Submarino sendo herdada de barcos.
class Submarino : public barcos{

private:
	Submarino();

public:
	Submarino(string NomeBarco, string OrientacaoBarco, string Mapa);
	~Submarino();
};

#endif