#ifndef BARCOS_HPP
#define BARCOS_HPP

#include "pch.hpp"
#include "mapa.hpp"

using namespace std;
//Classe abstrata para os tipos de barcos

class barcos{

private:
	int SizeBarco;
	vector<int> vida;
	vector<int> posicao;
	string NomeBarco;
	string OrientacaoBarco;
	string Map;

public:
	barcos(); //Metodo construtor.
	~barcos();//Metodo destrutor.

	//Metodos Auxiliares
	void set_SizeBarco(int SizeBarco);
	int get_SizeBarco();

	void set_vida(int vida);
	void set_vida(int VecPos, int vida);
	int  get_vida(int VecPos);
	unsigned int get_SizeVida();

	void set_posicao(int x, int y);
	void set_posicao(int VecPos, unsigned int value);
	vector<int> get_posicao();
	unsigned int get_SizePosicao();

	void set_NomeBarco(string NomeBarco);
	string get_NomeBarco();

	void set_OrientacaoBarco(string OrientacaoBarco);
	string get_OrientacaoBarco();
	
	//Outros Metodos
	void set_Mapa(string Map);
	string get_Mapa();

	void set_BarcosNoMapa(int posX, int posY, Mapa *Map);
};


#endif