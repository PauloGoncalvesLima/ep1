#ifndef CANOA_HPP
#define CANOA_HPP

#include "pch.hpp"
#include "barcos.hpp"

using namespace std;
//Classe Canoa sendo herdada de barcos.
class Canoa : public barcos{

private:
	Canoa();

public:
	Canoa(string NomeBarco, string OrientacaoBarco, string Mapa);
	~Canoa();
};

#endif