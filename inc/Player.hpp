#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "pch.hpp"

#include "PortaAvioes.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "mapa.hpp"


using namespace std;

class Player{

private:
	int VidaTotal;
	int QuantidadeDeBarcos;
	int Pontos;
	string NomeDoJogador;
	string MapaDoPlayer;
	Player();

public:
	Player(string MapaDoPlayer);
	~Player();

	void set_vidaTotal(int VidaTotal);
	int get_vidaTotal();

	void set_QuantidaDeDeBarcos(int QuantidadeDeBarcos);
	int get_QuantidadeDeBarcos();

	void set_Pontos(int Pontos);
	int get_Pontos();

	void set_NomeDoJogador(string NomeDoJogador);
	string get_NomeDoJogador();

	void set_MapaDoPlayer(string MapaDoPlayer);
	string get_MapaDoPlayer();

	void set_player(string QualPlayer, string arquivo, Mapa *map, vector<PortaAvioes *>& ListaDePortaAvioes, vector<Canoa *>& ListaDeCanoas, vector<Submarino *>& ListaDeSubmarinos);

	int Ataque(int y, int x, Mapa *map, Player *PlayerAtacado, string MapaAtacado,vector<PortaAvioes *>& ListaDePortaAvioes, vector<Canoa *>& ListaDeCanoas, vector<Submarino *>& ListaDeSubmarinos);
};	

#endif