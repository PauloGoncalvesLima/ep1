#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include "pch.hpp"
#include "barcos.hpp"
using namespace std;
//Classe PortaAvioes sendo herdada de barcos.
class PortaAvioes : public barcos{

private:
	PortaAvioes();

public:
	PortaAvioes(string NomeBarco, string OrientacaoBarco, string Mapa);
	~PortaAvioes();
};

#endif